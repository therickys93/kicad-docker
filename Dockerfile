FROM ware/kicad
RUN apt-get update && apt-get install --no-install-recommends kicad-symbols kicad-templates kicad-footprints kicad-libraries -y
